/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author VasMa
 */
public class AyudaController implements Initializable {

    @FXML
    private TextArea Ayuda;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void Registro(ActionEvent event) {
        Ayuda.setText("");
        Ayuda.setText("Preguntas Frecuentes \n No Hay datos \n No puedo agregar a los pacientes \n La computadora se Traba a la hora "
                + "de cargar los datos");
    }

    @FXML
    private void Chat(ActionEvent event) {
        Ayuda.setText("");
        Ayuda.setText("Preguntas Frecuentes \n No puedo enviar mensajes a el medico \n Se desconecta el chat repentinamente \n "
                + " ");
    }

    @FXML
    private void BusquedaPa(ActionEvent event) {
        Ayuda.setText("");
        Ayuda.setText("Preguntas Frecuentes \n No me redirije ningun dato \n   ");
    }

    @FXML
    private void Citas(ActionEvent event) {
        Ayuda.setText("");
        Ayuda.setText(" ");
    }
    
}
