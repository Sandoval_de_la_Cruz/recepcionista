/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Conexion.Citas;
import Conexion.Conection;
import Conexion.Medicos;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author MrKim
 */
public class Agendar_citaController implements Initializable {

    @FXML    private AnchorPane Anchor;
    @FXML    private DatePicker fecha;
    @FXML    private Button guardarcita;
    @FXML    private TextField asunto;
    @FXML    private JFXTextField idPaciente;
    @FXML    private ComboBox<Medicos> idMedicos;
    @FXML    private JFXTextField hora;
    @FXML    private TableView<Citas> tblCitas;
    @FXML    private TableColumn<Citas, Number> clmidPaciente;
    @FXML    private TableColumn<Citas, Number> clmidMedico;
    @FXML    private TableColumn<Citas, String> clmnombre;
    @FXML    private TableColumn<Citas, String> clmapellidoP;
    @FXML    private TableColumn<Citas, String> clmapellidoM;
    @FXML    private TableColumn<Citas, String> clmfecha;
    @FXML    private TableColumn<Citas, String> clmHora;
    private ObservableList<Citas> citas;    
    private ObservableList<Medicos> listaMedicos;    
    private Conection conexion;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        conexion = new Conection();
	conexion.establecerConexion();
        
        citas = FXCollections.observableArrayList();
        Citas.llenarInformacionAlumnos(conexion.getConnection(), citas);
        //.llenarInformacionAlumnos(conexion.getConnection(), agenda, nombreBusqueda.getText(), apellidoPBusqueda.getText(), apellidoMBusqueda.getText());
        tblCitas.setItems(citas);
        
        clmidPaciente.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idPaciente"));
        clmidMedico.setCellValueFactory(new PropertyValueFactory<Citas,Number>("idMedico"));
        clmnombre.setCellValueFactory(new PropertyValueFactory<Citas, String>("nombre"));
        clmapellidoP.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoP"));
        clmapellidoM.setCellValueFactory(new PropertyValueFactory<Citas, String>("apellidoM"));
        clmfecha.setCellValueFactory(new PropertyValueFactory<Citas, String>("fecha"));
        clmHora.setCellValueFactory(new PropertyValueFactory<Citas, String>("hora"));
        
        conexion.cerrarConexion();
    }    
    
}
