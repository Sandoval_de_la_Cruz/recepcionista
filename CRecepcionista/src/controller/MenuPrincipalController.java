/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author VasMa
 */
public class MenuPrincipalController implements Initializable {

    @FXML
    private AnchorPane menu;
    private AnchorPane vista;
    private AnchorPane inicio;
    @FXML
    private Label titulo;
    @FXML
    private AnchorPane Vistas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO              
        Pane ini=null;
        try {
            ini = FXMLLoader.load(getClass().getResource("/fileFXML/Inicio.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(MenuPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Vistas.getChildren().setAll(ini);
    }    



    @FXML    private void abrirInicio(ActionEvent event) throws IOException {
        titulo.setText("/Inicio");
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Inicio.fxml"));
        Vistas.getChildren().setAll(ini);
          menu.toBack();
    }

    @FXML    private void abrirPaciente(ActionEvent event) throws IOException {
         titulo.setText(null);
        titulo.setText("/Agregar Paciente");
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/RegistroPaciente.fxml"));
        Vistas.getChildren().setAll(ini);
          menu.toBack();
    }


    @FXML    private void abrirAgenda(ActionEvent event) throws IOException {
        titulo.setText(null);
        titulo.setText("/Agenda");
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Agendar_consulta.fxml"));
        Vistas.getChildren().setAll(ini);
          menu.toBack();
    }

    @FXML    private void cerrarMenu(ActionEvent event) {
        menu.toBack();
    }        

    @FXML
    private void mostrarmenu(ActionEvent event) {
        menu.toFront();
    }

    @FXML
    private void abrirBuscarPaciente(ActionEvent event) throws IOException {
        titulo.setText(null);
         titulo.setText("/Buscar Paciente");
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/BusquedaDePacientes.fxml"));
        Vistas.getChildren().setAll(ini);
          menu.toBack();
    }


    @FXML
    private void abrirAyuda(ActionEvent event) throws IOException {
        titulo.setText(null);
         titulo.setText("/Ayuda");
        Pane ini = FXMLLoader.load(getClass().getResource("/fileFXML/Ayuda.fxml"));
        Vistas.getChildren().setAll(ini);
          menu.toBack();
    }
    
}
