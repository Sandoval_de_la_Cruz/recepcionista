/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author VasMa
 */
public class Chat1Controller implements Initializable {

    @FXML
    private TextField mensaje;
    @FXML
    private TextArea mensajeria;
    @FXML
    private Button enviar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void EnviarMensaje(ActionEvent event) {
        String mensaje1 = mensaje.getText();
        if(mensaje1.equals("") || mensaje == null || mensaje1 == null){
            JOptionPane.showMessageDialog(null, "No se pudo enviar su mensaje");
        }else{
            this.mensaje.setText("");
            mensajeria.appendText("\n"+"Yo" + ":" + mensaje1);
        }
    }
    
}
