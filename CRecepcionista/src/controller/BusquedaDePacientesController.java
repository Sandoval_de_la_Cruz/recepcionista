/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Conexion.Conection;
import Conexion.Paciente;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author MrKim
 */
public class BusquedaDePacientesController implements Initializable {

    @FXML
    private TableView<Paciente> tblPacientes;
    @FXML
    private TableColumn<Paciente, Number> idPaciente;
    @FXML
    private TableColumn<Paciente, String> nombre;
    @FXML
    private TableColumn<Paciente, String> apellidoP;
    @FXML
    private TableColumn<Paciente, String> apellidoM;
    @FXML
    private TableColumn<Paciente, String> telefono;
    private ObservableList<Paciente> pacientes;    
    private Conection conexion;
    @FXML    private JFXTextField nombretext;
    @FXML    private JFXTextField apellidoPtext;
    @FXML    private JFXTextField apellidoMtext;
    @FXML    private JFXTextField idText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conexion = new Conection();
        conexion.establecerConexion();
        
        pacientes = FXCollections.observableArrayList();
        
        Paciente.llenarInformacionAlumnos(conexion.getConnection(), pacientes, nombretext.getText(),apellidoPtext.getText(), apellidoMtext.getText());
        tblPacientes.setItems(pacientes);
        
        idPaciente.setCellValueFactory(new PropertyValueFactory<Paciente,Number>("id"));
        nombre.setCellValueFactory(new PropertyValueFactory<Paciente,String>("nombre"));
        apellidoP.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoP"));
        apellidoM.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoM"));
        telefono.setCellValueFactory(new PropertyValueFactory<Paciente, String>("telefono"));
        conexion.cerrarConexion();
    }    
    @FXML    private void buscarporId(ActionEvent event) {
    }

    @FXML    private void buscarPorNombre(ActionEvent event) {
        if(nombretext.getText().equals("") || apellidoPtext.getText().equals("") || apellidoMtext.getText().equals("")){
            Alert dialogAlert2 = new Alert(Alert.AlertType.WARNING);
            dialogAlert2.setTitle("Advertencia");
            dialogAlert2.setContentText("Hay Campos Vacios");
            dialogAlert2.initStyle(StageStyle.UTILITY);
            dialogAlert2.showAndWait();
        }else{
            conexion = new Conection();
            conexion.establecerConexion();

            pacientes = FXCollections.observableArrayList();

            Paciente.llenarInformacionAlumnos(conexion.getConnection(), pacientes,nombretext.getText(),apellidoPtext.getText(), apellidoMtext.getText());
            tblPacientes.setItems(pacientes);

            idPaciente.setCellValueFactory(new PropertyValueFactory<Paciente,Number>("id"));
            nombre.setCellValueFactory(new PropertyValueFactory<Paciente,String>("nombre"));
            apellidoP.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoP"));
            apellidoM.setCellValueFactory(new PropertyValueFactory<Paciente, String>("apellidoM"));
            telefono.setCellValueFactory(new PropertyValueFactory<Paciente, String>("telefono"));
            conexion.cerrarConexion();
        }
        
    }
    
    public void gestionarEventos(){
		
	}
    
}
