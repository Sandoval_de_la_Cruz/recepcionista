/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Conexion.Conexion;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author MrKim
 */
public class RegistroPacienteController implements Initializable {

    @FXML
    private JFXTextField jTextField1;
    @FXML
    private JFXTextField jTextField2;
    @FXML
    private JFXTextField jTextField3;
    @FXML
    private JFXTextField jTextField4;
    @FXML
    private JFXTextField jTextField10;
    @FXML
    private JFXTextField jTextField11;
    @FXML
    private JFXTextField jTextField5;
    @FXML
    private JFXTextField jTextField6;
    @FXML
    private JFXTextField jTextField7;
    @FXML
    private JFXTextField jTextField8;
    @FXML
    private JFXTextField jTextField9;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
       // this.generarNumeracion();
    }    

    @FXML
    private void cancelRegistro(ActionEvent event) {
    }

    @FXML
    private void registroPaciente(ActionEvent event) {
        if(jTextField1.getAccessibleHelp().equals("") || jTextField2.getText().equals("") || jTextField3.getText().equals("")
                || jTextField4.getText().equals("") || jTextField5.getText().equals("") || jTextField6.getText().equals("")
                || jTextField7.getText().equals("") || jTextField8.getText().equals("") || jTextField9.getText().equals("")
                || jTextField10.getText().equals("") || jTextField11.getText().equals("")){               
                    Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
                    dialogAlert2.setTitle("Advertencia");
                    dialogAlert2.setContentText("Algun campo no esta lleno");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
        }else{
         /*   String sql = "INSERT INTO paciente() VALUES ()";
            try {
                PreparedStatement pst = cn.prepareStatement(sql);
                pst.setString(0, jTextField1.getText());
                pst.setString(1, jTextField2.getText());
                pst.setString(2, jTextField3.getText());
                pst.setString(3, jTextField4.getText());
                pst.setString(4, jTextField5.getText());
                pst.setString(5, jTextField6.getText());
                pst.setString(6, jTextField7.getText());
                pst.setString(7, jTextField8.getText());
                pst.setString(8, jTextField9.getText());
                pst.setString(9, jTextField10.getText());
                pst.setString(10, jTextField11.getText());
                int n = pst.executeUpdate();
                if(n>0){
                     Alert dialogAlert2 = new Alert(Alert.AlertType.INFORMATION);
                    dialogAlert2.setTitle("");
                    dialogAlert2.setContentText("Usuario Guardado Corecctamente");
                    dialogAlert2.initStyle(StageStyle.UTILITY);
                    dialogAlert2.showAndWait();
                    jTextField1.setText(null);jTextField2.setText(null);jTextField3.setText(null);jTextField4.setText(null);jTextField5.setText(null);
                    jTextField6.setText(null);jTextField7.setText(null);jTextField8.setText(null);jTextField9.setText(null);jTextField10.setText(null);
                    jTextField11.setText(null);
                }
                
                
                pst.executeUpdate();
                
                
            } catch (SQLException ex) {
                Logger.getLogger(RegistroPacienteController.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            
        }
    }
    /*
    void generarNumeracion(){
        String SQL="select max(idpaciente) from paciente";
            int b = 0;
            int c = 0;
        try {            
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);
            while(rs.next()){
                c = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RegistroPacienteController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    Conexion cc= new Conexion();
    Connection cn= cc.conexion();
}
