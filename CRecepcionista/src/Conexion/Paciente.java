/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Jose Pablo
 */
public class Paciente {
    
    private IntegerProperty id;
    private StringProperty nombre;
    private StringProperty apellidoP;
    private StringProperty apellidoM;
    private StringProperty telefono;

    public Paciente(Integer id, String nombre, String apellidoP, String apellidoM, String telefono) {
        this.id =new SimpleIntegerProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.apellidoP = new SimpleStringProperty(apellidoP);
        this.apellidoM = new SimpleStringProperty(apellidoM);
        this.telefono = new SimpleStringProperty(telefono);
    }
    public Integer getId() {
        return id.get();
    }

    public void setId(Integer id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    public String getApellidoP() {
        return apellidoP.get();
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP =new SimpleStringProperty (apellidoP);
    }

    public String getApellidoM() {
        return apellidoM.get();
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM =new SimpleStringProperty(apellidoM);
    }

    public String getTelefono() {
        return telefono.get();
    }

    public void setTelefono(String telefono) {
        this.telefono =new SimpleStringProperty(telefono);
    }

    public static void llenarInformacionAlumnos(Connection connection, ObservableList<Paciente> lista,
            String nombre, String apellidoP, String apellidoM) {
        String sql;
        try {
            if(nombre.equals("") || apellidoP.equals("") || apellidoM.equals("")){
                sql = "Select "
                    + "idPaciente, "
                    + "nombre, "
                    + "apellido_paterno, "
                    + "apellido_materno, "
                    + "telefono "
                    + "from bookmedic.paciente";
            }else{
                sql = "Select "
                        + "idPaciente, "
                        + "nombre, "
                        + "apellido_paterno, "
                        + "apellido_materno, "
                        + "telefono "
                        + "from bookmedic.paciente "
                        + "where nombre = '"+ nombre+"' "
                        + "and apellido_paterno = '"+ apellidoP+"' "
                        + "and apellido_materno = '"+ apellidoM+"'";
            }
            
            //+ "where fecha = '" + Fecha()+"'";
            Statement st = connection.createStatement();
            ResultSet resultado = st.executeQuery(sql);
            //Statement instruccion = connection.createStatement();
            //ResultSet resultado = instruccion.executeQuery();
            while (resultado.next()) {
                lista.add(
                        new Paciente(
                                resultado.getInt("idPaciente"),
                                resultado.getString("nombre"),
                                resultado.getString("apellido_paterno"),
                                resultado.getString("apellido_materno"),
                                resultado.getString("telefono")
                        //resultado.getString("hora")
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
