/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 *
 * @author Jose Pablo
 */
public class Citas {
        private IntegerProperty idPaciente;
        private IntegerProperty idMedico;
	private StringProperty nombre;
	private StringProperty apellidoP;
        private StringProperty apellidoM;
	private StringProperty fecha;
	private StringProperty hora;
	
        
        public Citas(Integer idPaciente,Integer idMedico, String nombre, String apellidoP, String apellidoM, String fecha){
            this.idPaciente = new SimpleIntegerProperty(idPaciente);
            this.idMedico = new SimpleIntegerProperty(idMedico);
            this.nombre = new SimpleStringProperty(nombre);
            this.apellidoP = new SimpleStringProperty(apellidoP);
            this.apellidoM = new SimpleStringProperty(apellidoM);
            this.fecha = new SimpleStringProperty(fecha);
            //this.hora = new SimpleStringProperty(hora);            
        }    

    public Integer getIdPaciente() {
        return idPaciente.get();
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = new SimpleIntegerProperty(idPaciente);
    }

    public Integer getIdMedico() {
        return idMedico.get();
    }

    public void setIdMedico(Integer idMedico) {
        this.idMedico = new SimpleIntegerProperty(idMedico);
    }

    public String getNombre() {
        return nombre.get();
    }

    public void setNombre(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
    }

    public String getApellidoP() {
        return apellidoP.get();
    }

    public void setApellidoP(String apellidoP) {
        this.apellidoP = new SimpleStringProperty(apellidoP);
    }

    public String getApellidoM() {
        return apellidoM.get();
    }

    public void setApellidoM(String apellidoM) {
        this.apellidoM = new SimpleStringProperty(apellidoM);
    }

    public String getFecha() {
        return fecha.get();
    }

    public void setFecha(String fecha) {
        this.fecha = new SimpleStringProperty(fecha);
    }


    public StringProperty getHora() {
        return hora;
    }

    public void setHora(StringProperty hora) {
        this.hora = hora;
    }
        
   public static void llenarInformacionAlumnos(Connection connection,ObservableList<Citas> lista){
       String sql="";
		try {                                                                 
                        sql="select "
                                + "citas.idpaciente,"
                                + "medico.idMedico,"
                                + "paciente.nombre, "
                                + "paciente.apellido_paterno, "
                                + "paciente.apellido_materno, "
                                + "fecha "
                                + "from bookmedic.citas inner join bookmedic.paciente "
                                + "on paciente.idpaciente = citas.idpaciente "
                                + "inner join bookmedic.medico "
                                + "on citas.idMedico = medico.idMedico";                             
                                        
			Statement st = connection.createStatement();
                        ResultSet resultado = st.executeQuery(sql);                                                
			while(resultado.next()){                            
				lista.add(
						new Citas(
								resultado.getInt("idpaciente"),
                                                                resultado.getInt("idMedico"),
								resultado.getString("nombre"),
								resultado.getString("apellido_paterno"),
                                                                resultado.getString("apellido_materno"),
                                                                resultado.getString("fecha")
                                                                //resultado.getString("hora")
						)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}                
}
